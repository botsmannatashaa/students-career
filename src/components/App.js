import "../styles/App.css";

import { Routes, Route, Link } from "react-router-dom";
import React, { useState, useEffect } from "react";

import Header from "./Header";
import Main from "./Main";
import Student from "./SingleStudentPage";
import Frontend from "./Frontend";
import Backend from "./Backend";
import Design from "./Design";
import Mobile from "./MobileDevs";

function App() {
  return (
    <div className="App">
      <>
        <Header />
        <Routes>

          <Route path="/" exact={true} element={<Main />} />

          <Route
            path="/details/:collection/:id"
            exact={true}
            element={<Student />}
          />
          <Route
            path="/details/frontend developers"
            exact={true}
            element={<Frontend />}
          />

          <Route 
            path="/details/backend developers" 
            exact={true} 
            element={<Backend />} 
          />

          <Route
            path="/details/designers"
            exact={true}
            element={<Design />}
          />

          <Route
            path="/details/mobile developers"
            exact={true}
            element={<Mobile/>}
          />

        </Routes>
      </>
    </div>
  );
}

export default App;
