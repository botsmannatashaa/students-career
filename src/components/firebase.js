import firebase from "firebase/compat/app";
import "firebase/compat/auth";
import "firebase/compat/firestore";
import { initializeApp } from "firebase/app";

const firebaseConfig = {
  apiKey: "AIzaSyDgpDHrbr175IAOl-NwRft6bTVWabtoK7c",
  authDomain: "it-academy-students.firebaseapp.com",
  projectId: "it-academy-students",
  storageBucket: "it-academy-students.appspot.com",
  messagingSenderId: "428404251886",
  appId: "1:428404251886:web:0409e5c815b27d7463a7ff",
  measurementId: "G-DXVMQS14J3"
};

firebase.initializeApp(firebaseConfig);
export const firestore = firebase.firestore();
export const app = initializeApp(firebaseConfig);

export default firebase;