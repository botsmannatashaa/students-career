import { Link } from 'react-router-dom';
import '../styles/Header.css';

export default function Header () {
    return (
        <>
        <div className='header'>
            <Link to={'/'}>
            <img 
            className='logo'
            src='../logo192.png'/>
            </Link>

        </div>
        </>
    );
}