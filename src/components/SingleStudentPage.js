import React, { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import firebase from "./firebase";

export default function Student() {
  const { collection, id } = useParams();
  const [data, setData] = useState(null);

  useEffect(() => {
    const db = firebase.firestore();
    db.collection(collection)
      .doc(id)
      .get()
      .then((doc) => {
        if (doc.exists) {
          setData(doc.data());
        } else {
          console.log("Документ не найден");
        }
      })
      .catch((error) => {
        console.error("Ошибка получения данных:", error);
      });
  }, [collection, id]);

  if (!data) {
    return <p>Loading...</p>;
  }

  return (
    <div>
      <img src={data.image} />
      <h2>{data.name}</h2>
      <h3>{data.age}</h3>
      <p>{data.technology}</p>
      <p>{data.experience}</p>
      <p>{data.softSkills}</p>
      <p>{data.hardSkills}</p>
    </div>
  );
}
