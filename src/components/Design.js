import React, { useEffect, useState } from "react";
import { Link } from "react-router-dom";

import "../styles/StudentsCards.css";

import firebase from "./firebase";
import { collection, getDocs } from "firebase/firestore";
import { QuerySnapshot } from "firebase/firestore"

export default function Design() {
  const [data, setData] = useState([]);
  const design = "Designers"; 

  useEffect(() => {
    const db = firebase.firestore();

    const fetchData = async () => {
      const querySnapshot = await db.collection(design).get();

      const allData = querySnapshot.docs.map((doc) => ({
        id: doc.id,
        ...doc.data(),
      }));

      setData(allData);
    };

    fetchData();
  }, []);

  return (
    <>
      <div>
        <h2>Designers</h2>
      </div>
      <div className="outerWrapper">
        {data.map((item) => (
          <Link
            className="linksDec"
            to={`/details/${design}/${item.id}`}
            key={item.id}
          >
            <div className="innerWrapper">
              <img className="cardImg" src={item.image} alt="Student" />
              <h2>{item.name}</h2>
              <h4>{item.technology}</h4>
            </div>
          </Link>
        ))}
      </div>
    </>
  );
}
