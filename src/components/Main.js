import React, { useEffect, useState } from "react";
import { Link } from "react-router-dom";

import "../styles/Main.css";

import firebase from "./firebase";
import { collection, getDocs } from "firebase/firestore";

export default function Main() {
  const [data, setData] = useState([]);
  const collections = [
    "Frontend Developers",
    "Backend Developers",
    "Designers",
    "Mobile Developers",
  ];
  
  useEffect(() => {
    const fetchData = async () => {
      const db = firebase.firestore();
      const allData = [];

      for (const collectionName of collections) {
        const querySnapshot = await db.collection(collectionName).get();

        querySnapshot.forEach((doc) => {
          allData.push({ id: doc.id, collectionName, ...doc.data() });
        });
      }

      setData(allData);
    };

    fetchData();
  }, []);

  const groupedData = data.reduce((acc, item) => {
    if (!acc[item.collectionName]) {
      acc[item.collectionName] = [];
    }
    acc[item.collectionName].push(item);
    return acc;
  }, {});

  return (
    <div className="outerDiv">
      {Object.keys(groupedData).map((collectionName) => (
        <div className="collection" key={collectionName}>
          <Link to={`/details/${collectionName.toLowerCase()}`} className="txtDecoration">
            <h2 className="collectionName">{collectionName}</h2>
          </Link>
          <div className="collectionItems">
            {groupedData[collectionName].map((item) => (
              <Link
                className="txtDecoration"
                to={`/details/${item.collectionName}/${item.id}`}
                key={item.id}
              >
                <div className="innerDiv">
                  <img className="studImg" src={item.image} alt="Student" />
                  <h2>{item.name}</h2>
                  <h4>{item.technology}</h4>
                </div>
              </Link>
            ))}
          </div>
        </div>
      ))}
    </div>
  );
}
